package com.facelook.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.ManyToOne;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findEventsByNameLike", "findEventsByInvitees", "findEventsByOrganisedBy" })
public class Event {

    /**
     */
    @NotNull
    private String name;

    /**
     */
    @NotNull
    private String location;

    /**
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar datetime;

    /**
     */
    @NotNull
    private String starttime;

    /**
     */
    @NotNull
    private String endtime;

    /**
     */
    @NotNull
    private String description;

    /**
     */
    private String driver;

    /**
     */
    @NotNull
    private Float latitude;

    /**
     */
    @NotNull
    private Float longitude;

    /**
     */
    private Float latitudesp;

    /**
     */
    private Float longitudesp;

    /**
     */
    private Float latitudewp1;

    /**
     */
    private Float longitudewp1;

    /**
     */
    private Float latitudewp2;

    /**
     */
    private Float longitudewp2;

    /**
     */
    private Float latitudewp3;

    /**
     */
    private Float longitudewp3;

    /**
     */
    @ManyToOne
    private UserRegistration organisedBy;

    /**
     */
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Comment> comments = new HashSet<Comment>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Interest> interests = new HashSet<Interest>();

    /**
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
    private Set<Invite> invites = new HashSet<Invite>();

    @AssertTrue(message = "Please reselect a start time and an end time! Ensure end time is greater than the start time.")
    @Transient
    public Boolean isValid() {
        String start = starttime.replace("T", "");
        String replaceS = start.replaceAll(":", "");
        int startT = Integer.parseInt(replaceS);
        String end = endtime.replace("T", "");
        String replaceE = end.replaceAll(":", "");
        int endT = Integer.parseInt(replaceE);
        if (endT - startT <= 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     */
    private String googleCalendarId;

    private byte[] image;
    private String contentType;
}
