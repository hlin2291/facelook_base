// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.facelook.domain;

import com.facelook.domain.Invite;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect Invite_Roo_Jpa_Entity {
    
    declare @type: Invite: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Invite.id;
    
    @Version
    @Column(name = "version")
    private Integer Invite.version;
    
    public Long Invite.getId() {
        return this.id;
    }
    
    public void Invite.setId(Long id) {
        this.id = id;
    }
    
    public Integer Invite.getVersion() {
        return this.version;
    }
    
    public void Invite.setVersion(Integer version) {
        this.version = version;
    }
    
}
