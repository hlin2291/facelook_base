package com.facelook.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findCommentsByCommentForEvent" })
public class Comment {

    /**
     */
    @NotNull
    private String description;

    /**
     */
    @ManyToOne
    private UserRegistration RegisteredUser;

    /**
     */
    @OneToOne
    private UserRegistration madeBy;

    /**
     */
    @ManyToOne
    private Event commentForEvent;
}
