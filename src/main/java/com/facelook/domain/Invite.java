package com.facelook.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findInvitesByEvent", "findInvitesByInvitee", "findInvitesByEventAndInvitee", "findInvitesByEventAndAttending" })
public class Invite {

    /**
     */
    @ManyToOne
    private Event event;

    /**
     */
    @ManyToOne
    private UserRegistration invitee;

    /**
     */
    private Boolean attending;
}
