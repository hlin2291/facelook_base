package com.facelook.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;










import flexjson.JSONSerializer;
public class JsonSerialize {
 public String JsonSerializerCreate(String summary, String location, Calendar date, String starttime, String endtime,String timeZone) {
    	 
	 JSONSerializer serializer = new JSONSerializer();

     Map<String, Object> data = new HashMap<String, Object>();
     HashMap <String,String>start = DatetimeFormat(date, starttime, timeZone);
     System.out.println(start);
     HashMap <String,String>finish = DatetimeFormat(date, endtime, timeZone);
     data.put("start",start );
     data.put("end", finish);
     data.put("location", location);
     data.put("summary", summary);
     String flexJsonString = serializer.serialize(data);
     System.out.println(flexJsonString);
     //System.out.println("me here");
     return flexJsonString;
    	 
     }
 public HashMap<String,String> DatetimeFormat (Calendar date,String timeEvent,String timeZone){
	 HashMap dateTime =new HashMap();
	String year =Integer.toString(date.get(Calendar.YEAR));
	
	String month = Integer.toString(date.get(Calendar.MONTH)+1);
	if(month.length()<2){
		month="0"+month;
	}
	String day = Integer.toString(date.get(Calendar.DATE));
	if(day.length()<2){
		day="0"+day;
	}
	dateTime.put("dateTime", year+"-"+month+"-"+day+"T"+timeEvent+".00");
	dateTime.put("timeZone",timeZone);
	 
	return dateTime;
	 
 }
 public String JsonSerializerUpdate(HashSet<String>useremails,String summary, String location, Calendar date, String starttime, String endtime,String timeZone) {
	 
	 JSONSerializer serializer = new JSONSerializer();

    

     
     Map<String,Object> data = new HashMap<String, Object>();
     ArrayList attendees = new ArrayList();
     if (useremails!=null){
     for(String useremail : useremails) {
      HashMap email =new HashMap();
      email.put("email", useremail);
      attendees.add(email);
      System.out.println(email);
      data.put("attendees",attendees);
     }
     }
     HashMap <String,String>start = DatetimeFormat(date, starttime, timeZone);
     HashMap <String,String>finish = DatetimeFormat(date, endtime, timeZone);
     data.put("start",start );
     data.put("end", finish);
     data.put("location", location);
     data.put("summary", summary);
     
     System.out.println(data);
     String flexJsonString = serializer.deepSerialize(data);
     System.out.println(flexJsonString);
     //System.out.println("me here");
     return flexJsonString;
    	 
     }

}
