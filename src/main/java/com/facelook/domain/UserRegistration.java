package com.facelook.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.persistence.Column;
import java.util.Calendar;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.TypedQuery;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findUserRegistrationsByAgeBetween", "findUserRegistrationsByEmailEquals" })
public class UserRegistration {

    /**
     */
    @NotNull
    private String firstname;

    /**
     */
    @NotNull
    private String lastname;

    /**
     */
    @NotNull
    @Column(unique = true)
    private String email;

    /**
     */
    @NotNull
    private Integer postcode;

    /**
     */
    private Float latitude;

    /**
     */
    private Float longitude;

    /**
     */
    @NotNull
    private String password;

    /**
     */
    private String facebookId;

    /**
     */
    private String twitterId;

    /**
     */
    @NotNull
    private Boolean authenticated;

    /**
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar dateofbirth;

    /**
     */
    @NotNull
    private String phonenumber;

    /**
     */
    private Integer age;

    /**
     */
    private String roleName;

    /**
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organisedBy")
    private Set<Event> organised = new HashSet<Event>();

    /**
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invitee")
    private Set<Invite> invites = new HashSet<Invite>();

    public static TypedQuery<UserRegistration> findUserRegistrationsToInviteToEvent(Event event) {
        if (event == null) throw new IllegalArgumentException("The event argument is required");
        EntityManager em = UserRegistration.entityManager();
        TypedQuery<UserRegistration> q = em.createQuery("SELECT o FROM UserRegistration o WHERE o.id NOT IN (SELECT i.invitee FROM Invite i WHERE i.event = :event) AND o != :userregistration", UserRegistration.class);
        q.setParameter("event", event);
        q.setParameter("userregistration", event.getOrganisedBy());
        return q;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Interest> interests = new HashSet<Interest>();

    public static TypedQuery<UserRegistration> findUserRegistrationsByEmailEquals(String email) {
        if (email == null || email.length() == 0) throw new IllegalArgumentException("The email argument is required");
        EntityManager em = UserRegistration.entityManager();
        TypedQuery<UserRegistration> q = em.createQuery("SELECT o FROM UserRegistration AS o WHERE o.email = :email", UserRegistration.class);
        q.setParameter("email", email);
        return q;
    }


    /**
     */
    private String refreshToken;

    /**
     */
    private Boolean acccess;

    /**
     */
    private String gmailAccount;
}
