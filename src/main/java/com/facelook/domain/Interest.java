package com.facelook.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Interest {

    /**
     */
    @NotNull
    private String name;

    /**
     */
    @NotNull
    private String description;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "interests")
    private Set<UserRegistration> users = new HashSet<UserRegistration>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "interests")
    private Set<Event> events = new HashSet<Event>();
}
