package com.facelook.domain;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.Principal;
import java.util.Calendar;
import java.util.HashSet;

import javax.persistence.TypedQuery;

public class SendHttpRequest {
public String[] getAccessToken (String request,String parameter) throws IOException{
		
		
		HttpURLConnection connection =getConnection ( "POST", request,1 );
		connection.setRequestProperty("Content-Length", "" + Integer.toString(parameter.getBytes().length));
		connection.setUseCaches (false);
		//GET https://www.googleapis.com/oauth2/v1/userinfo?access_token=1/fFBGRNJru1FQd44AzqT3Zg
		
		DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
		wr.writeBytes(parameter);
		wr.flush();
		wr.close();
		

		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		 JsonReader read = new JsonReader();
		String [] tokens= read.Reader(in );

		//aSystem.out.print("me~~"+ userdetails.getRefreshToken()+refreshtoken);
		
		in.close();
		
		connection.disconnect();
		return tokens;
	}
	public  String[] getCalendar (String request ) throws IOException{
		HttpURLConnection connection =getConnection ( "GET", request ,1);
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		 JsonReader read = new JsonReader();
	
		String[] readMe=read.Reader2(in);
		
		in.close();
		
		connection.disconnect();
		 return readMe ;
	}
	public HttpURLConnection getConnection (String RequestMethod,String request ,int i) throws IOException{
		URL url = new URL(request); 
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false); 
		connection.setRequestMethod(RequestMethod); 
		if (i == 1){
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
		connection.setRequestProperty("charset", "utf-8");
		}
		else{
			connection.setRequestProperty("Content-Type", "application/json");
			
		}
		return connection;
		
	}
	
	public   String addEvents ( String starttime, String endtime, String accesstoken,Calendar date, String summary,String location,String timeZone ) throws IOException{
		StringBuilder response = new StringBuilder(); 
		String id;
		URL url = new URL("https://www.googleapis.com/calendar/v3/calendars/primary/events?access_token="+accesstoken);
		  HttpURLConnection conn = null;
		  try {
		    conn = (HttpURLConnection) url.openConnection();
		      conn.setRequestMethod("POST");
		      conn.setDoOutput(true); 
		      conn.setDoInput(true); 
		      conn.setUseCaches(false); 
		      conn.setAllowUserInteraction(false);
		      conn.setRequestProperty("Content-Type","application/json");
		    // Open a stream which can write to the URL******************************************
		    OutputStream out = conn.getOutputStream();
		    OutputStreamWriter wr = null;
		    try {
		      wr = new OutputStreamWriter(out);
		      JsonSerialize JsonS = new JsonSerialize();
			String JsonString = JsonS.JsonSerializerCreate(summary, location, date, starttime, endtime,timeZone);
		      wr.write(JsonString); //ezm is my JSON object containing the api commands
		    }
		    finally { //in this case, we are ensured to close the output stream
		      if (wr != null)
		        wr.close();
		    }
		    // Open a stream which can read the server response*********************************
		    InputStream in = conn.getInputStream();
		    BufferedReader rd = null;
		    try {
		      rd = new BufferedReader(new InputStreamReader(in));
		      JsonReader idReader = new JsonReader();
		     id = idReader.IdReader(rd);
		      String responseSingle = null;
		      while ((responseSingle = rd.readLine()) != null) {
		        response.append(responseSingle);
		      }
		      
		      //println("The server response is " + response);
		    }
		    finally {  //in this case, we are ensured to close the input stream
		      if (rd != null)
		        rd.close();
		    }
		  }
		  finally {  //in this case, we are ensured to close the connection itself
		    if (conn != null)
		      conn.disconnect();
		  }
		return id;
		
	}
	
	public  void attendEvents ( String accesstoken,String id,HashSet userEmails,String summary, String location, Calendar date, String starttime, String endtime,String timeZone) throws IOException{
		StringBuilder response = new StringBuilder(); 
		
		URL url = new URL("https://www.googleapis.com/calendar/v3/calendars/primary/events/"+id+"?access_token="+accesstoken);
		  HttpURLConnection conn = null;
		  try {
		    conn = (HttpURLConnection) url.openConnection();
		      conn.setRequestMethod("PUT");
		      conn.setDoOutput(true); 
		      conn.setDoInput(true); 
		      conn.setUseCaches(false); 
		      conn.setAllowUserInteraction(false);
		      conn.setRequestProperty("Content-Type","application/json");
		    // Open a stream which can write to the URL******************************************
		    OutputStream out = conn.getOutputStream();
		    OutputStreamWriter wr = null;
		    try {
		      wr = new OutputStreamWriter(out);
		      JsonSerialize JsonS = new JsonSerialize();
			String JsonString = JsonS.JsonSerializerUpdate(userEmails, summary, location, date, starttime, endtime, timeZone);
		      wr.write(JsonString); //ezm is my JSON object containing the api commands
		    }
		    finally { //in this case, we are ensured to close the output stream
		      if (wr != null)
		        wr.close();
		    }
		    // Open a stream which can read the server response*********************************
		    InputStream in = conn.getInputStream();
		    BufferedReader rd = null;
		    try {
		      rd = new BufferedReader(new InputStreamReader(in));
		     
		      String responseSingle = null;
		      while ((responseSingle = rd.readLine()) != null) {
		        response.append(responseSingle);
		      }
		      
		      //println("The server response is " + response);
		    }
		    finally {  //in this case, we are ensured to close the input stream
		      if (rd != null)
		        rd.close();
		    }
		  }
		  finally {  //in this case, we are ensured to close the connection itself
		    if (conn != null)
		      conn.disconnect();
		  }
		
		
	}
	
	public String accessTokencall(String refreshtoken) throws IOException{
		  String urlParameters = "client_id=110258701403.apps.googleusercontent.com&client_secret=QMZ8P3hZJ8z8z4NcIiI-hLSY&refresh_token="+refreshtoken+"&grant_type=refresh_token";
				
				
			String request = "https://accounts.google.com/o/oauth2/token";
			SendHttpRequest Hrequest = new SendHttpRequest();
			
			
			String[] tokens=Hrequest.getAccessToken ( request,urlParameters);
			String accesstoken = tokens[0];
			return accesstoken;
		
	}
	public String []dataRequest(String accessToken) throws IOException{
		String requestdata ="https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token="+accessToken;
		    
		String [] requestedData =getCalendar(requestdata);
		return requestedData;
	}
	
}
