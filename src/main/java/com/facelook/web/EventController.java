package com.facelook.web;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;

import com.facelook.domain.Comment;
import com.facelook.domain.Event;
import com.facelook.domain.GoogleAdd;
import com.facelook.domain.Interest;
import com.facelook.domain.Invite;
import com.facelook.domain.UserRegistration;

import org.apache.commons.io.IOUtils;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.facelook.domain.SendHttpRequest;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;

@RequestMapping("/events")
@Controller

@RooWebScaffold(path = "events", formBackingObject = Event.class,delete=false, update=true)
public class EventController {

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
	public String create(@Valid Event event, @RequestParam("image") MultipartFile multipartFile, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) throws IOException {
        event.setStarttime(event.getStarttime().substring(1));
        event.setEndtime(event.getEndtime().substring(1));
        
		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, event);
			System.out.println(bindingResult);
			uiModel.addAttribute("error", bindingResult.getAllErrors().get(0)
					.getDefaultMessage());
			return "events/create";
		}
		uiModel.asMap().clear();
		String currentUser = principal.getName();

		event.setOrganisedBy(UserRegistration.findUserRegistrationsByEmailEquals(currentUser).getSingleResult());
        event.setContentType(multipartFile.getContentType());

		// 1/faveJoK9IACMP3Rw7eJVPc0SL71UL_ciWX_9cyvDKWg

		SendHttpRequest Hrequest = new SendHttpRequest();
		TypedQuery<UserRegistration> userdetails = UserRegistration
				.findUserRegistrationsByEmailEquals(currentUser);
		String[] userData;
		if (userdetails.getSingleResult().getRefreshToken() == null||userdetails.getSingleResult().getRefreshToken().equals("")) {
			String aTokenS = Hrequest
					.accessTokencall("1/faveJoK9IACMP3Rw7eJVPc0SL71UL_ciWX_9cyvDKWg");
			// String
			// aTokenU=Hrequest.accessTokencall(userdetails.getSingleResult().getRefreshToken());
			userData = Hrequest.dataRequest(aTokenS);
            System.out.println( event.getDatetime());
			String id = Hrequest.addEvents(event.getStarttime(),
					event.getEndtime(), aTokenS, event.getDatetime(),
					event.getDescription(), event.getLocation(), userData[0]);
			event.setGoogleCalendarId(id);
		} else {
			String aTokenS = Hrequest
					.accessTokencall("1/faveJoK9IACMP3Rw7eJVPc0SL71UL_ciWX_9cyvDKWg");
			String aTokenU = Hrequest.accessTokencall(userdetails
					.getSingleResult().getRefreshToken());
			userData = Hrequest.dataRequest(aTokenS);

			String id = Hrequest.addEvents(event.getStarttime(),
					event.getEndtime(), aTokenS, event.getDatetime(),
					event.getDescription(), event.getLocation(), userData[0]);
			event.setGoogleCalendarId(id);
			
			HashSet<String> emails = new HashSet<String>();
			emails.add(userdetails.getSingleResult().getGmailAccount());
			Hrequest.attendEvents(aTokenS, id, emails, event.getDescription(), event.getLocation(), event.getDatetime(), event.getStarttime(), event.getEndtime(), userData[0]);
		}
		event.persist();
		return "redirect:/events/"
				+ encodeUrlPathSegment(event.getId().toString(),
						httpServletRequest);
	}

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel, @ModelAttribute("comment") Comment comment) {
        //addDateTimeFormatPatterns(uiModel);
        Event event = Event.findEvent(id);
        event.setInvites(new HashSet<Invite>(Invite.findInvitesByEvent(event).getResultList()));
        uiModel.addAttribute("userregistrations", UserRegistration.findUserRegistrationsToInviteToEvent(event).getResultList());
        uiModel.addAttribute("event", event);
        uiModel.addAttribute("itemId", id);
        List<Comment> comments = Comment.findCommentsByCommentForEvent(Event.findEvent(id)).getResultList();
        uiModel.addAttribute("comments", comments);
        return "events/show";
    }
    
    @RequestMapping(value = "/invite", produces = "text/html")
    public String invite(@RequestParam("event") Long eventId, @RequestParam("invitee") Long inviteeId, Model uiModel) {
        Invite invite = new Invite();
        invite.setEvent(Event.findEvent(eventId));
        invite.setInvitee(UserRegistration.findUserRegistration(inviteeId));
        invite.persist();
        return "redirect:/events/" + eventId;
    }

    @RequestMapping(value = "/uninvite/{id}", produces = "text/html")
    public String uninvite(@PathVariable("id") Long id, Model uiModel) {
        Invite invite = Invite.findInvite(id);
        invite.remove();
        uiModel.asMap().clear();
        return "redirect:/events/" + invite.getEvent().getId();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/addcomment/{id}", produces = "text/html")
    public String addComment(@PathVariable("id") Long id, Model uiModel, @ModelAttribute("comment") Comment comment, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("comment", comment);
            return "redirect:/events/" + encodeUrlPathSegment(comment.getId().toString(), httpServletRequest);
        }
        //uiModel.addAttribute("event", Event.findEvent(id));
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        TypedQuery<UserRegistration> userdetails = UserRegistration.findUserRegistrationsByEmailEquals(username);
        comment.setMadeBy(userdetails.getSingleResult());
        comment.setCommentForEvent(Event.findEvent(id));
        uiModel.asMap().clear();
        comment.persist();
        return "redirect:/events/" + encodeUrlPathSegment(id.toString(), httpServletRequest);
    }

    void populateEditForm(Model uiModel, Event event) {
        uiModel.addAttribute("event", event);
        addDateTimeFormatPatterns(uiModel);
        if (event.getId() != null) {
            uiModel.addAttribute("invites", Invite.findInvitesByEvent(event).getResultList());
            uiModel.addAttribute("userregistrations", UserRegistration.findUserRegistrationsToInviteToEvent(event).getResultList());
        }
        uiModel.addAttribute("interests", Interest.findAllInterests());
    }

    @RequestMapping(value = "/{id}/image", method = RequestMethod.GET)
    public String showImage(@PathVariable("id") Long id, HttpServletResponse response, Model model) {
        Event event = Event.findEvent(id);
        if (event != null) {
            byte[] image = event.getImage();
            if (image != null) {
                try {
                    response.setContentType(event.getContentType());
                    OutputStream out = response.getOutputStream();
                    IOUtils.copy(new ByteArrayInputStream(image), out);
                    out.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws ServletException {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {
        }
        return pathSegment;
    }
	
	
	
//	  public static List<Event> findEventEntriesForUser(int firstResult, int maxResults) {
//	        return new Event().entityManager.createQuery("select e.* from event e, site_member s, event_site_members es WHERE e.id = es.event AND es.site_members = s.id AND s.username = ?", Event.class).setParameter(0, "david").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
//	    }

    @RequestMapping(value = "/{id}", params = "attending",produces = "text/html")
    public String showEvent(@PathVariable("id") Long id, Model uiModel,Principal principal) throws IOException {
        addDateTimeFormatPatterns(uiModel);
        
       Event event =  Event.findEvent(id);

		uiModel.addAttribute("event", event);
        uiModel.addAttribute("itemId", id);
        String currentUser = principal.getName();
        TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(currentUser);
        
        TypedQuery<Invite> invite = Invite.findInvitesByEventAndInvitee(event, userdetails.getSingleResult());
   	  // Boolean check = !invite.getResultList().isEmpty() && invite.getSingleResult().getAttending() == true;
      //   	if (check){
        	
      //   		uiModel.addAttribute("check", true);
      //   	}else{
        		
      //   		uiModel.addAttribute("check", false);
      //   	}
      //   	System.out.println(check);
        if (invite.getResultList().isEmpty()) {
            uiModel.addAttribute("attending", null);
        } else {
            uiModel.addAttribute("attending", invite.getSingleResult().getAttending());
        }
        
        	
    		
    		if (userdetails.getSingleResult().getRefreshToken()==null||userdetails.getSingleResult().getRefreshToken().equals("")){
    			uiModel.addAttribute("src","");
    		}
    		else{
    		SendHttpRequest Hrequest = new SendHttpRequest();
    		String aToken=Hrequest.accessTokencall( userdetails.getSingleResult().getRefreshToken());
    		String [] userData=Hrequest.dataRequest(aToken);
    			

           String timeZ=userData[0];
           String src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTz=0&amp;height=300&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src="+userData[1].replace("@", "%40")+"&amp;color=%232952A3&amp;ctz="+timeZ.replace("/", "%2F") ;
           uiModel.addAttribute("src",src);
    		}
        return "events/showEvent";
    }
    
    @RequestMapping(value = "/attend/{id}", produces = "text/html")
    public String updateAttendance(@PathVariable("id") Long id, Model uiModel, HttpServletRequest httpServletRequest,Principal principal) throws IOException {
    	Event event =  Event.findEvent(id);
    	final String currentUser = principal.getName();
		TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(currentUser);
	
	     
	     TypedQuery<Invite> ownInvite = Invite.findInvitesByEventAndInvitee(event, userdetails.getSingleResult());
		if (ownInvite.getResultList().isEmpty()) {
			Invite newInvite = new Invite();
			newInvite.setEvent(event);
			newInvite.setInvitee(userdetails.getSingleResult());
			newInvite.setAttending(true);
			newInvite.persist();
		} else {
		    ownInvite.getSingleResult().setAttending(true);
			ownInvite.getSingleResult().persist();
		}
	     
		GoogleAdd addMe = new GoogleAdd();
		addMe.add(event, userdetails.getSingleResult());

		  
		
        return "redirect:/events";
    }
   
    @RequestMapping(value = "/unattend/{id}", produces = "text/html")
    public String unAttend(@PathVariable("id") Long id, Model uiModel, HttpServletRequest httpServletRequest,Principal principal) throws IOException {
    	Event event =  Event.findEvent(id);
    	final String currentUser = principal.getName();
		TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(currentUser);
		 HashSet<UserRegistration> userset = new HashSet();
		 

	    
	     
	     TypedQuery<Invite> ownInvite = Invite.findInvitesByEventAndInvitee(event, userdetails.getSingleResult());
		if (ownInvite.getResultList().isEmpty()) {
			
		} else {
		    ownInvite.getSingleResult().setAttending(false);
			ownInvite.getSingleResult().persist();
		}
	
	
		
		GoogleAdd removeMe = new GoogleAdd();
		removeMe.remove(event, userdetails.getSingleResult());
		event.persist();
        return "redirect:/events";
    }

	@RequestMapping(value = "/updateAsOrganiser{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel,Principal principal) throws IOException {
        populateEditForm(uiModel, Event.findEvent(id));
    	TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(principal.getName());
		
		if (userdetails.getSingleResult().getRefreshToken()==null||userdetails.getSingleResult().getRefreshToken().equals("")){
			uiModel.addAttribute("src","");
		}
		else{
		SendHttpRequest Hrequest = new SendHttpRequest();
		String aToken=Hrequest.accessTokencall( userdetails.getSingleResult().getRefreshToken());
		String [] userData=Hrequest.dataRequest(aToken);
			

       String timeZ=userData[0];
       String src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTz=0&amp;height=300&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src="+userData[1].replace("@", "%40")+"&amp;color=%232952A3&amp;ctz="+timeZ.replace("/", "%2F") ;
       uiModel.addAttribute("src",src);
		}
        return "events/update";
    }
	@RequestMapping(value = "/{id}",params = "form",method = RequestMethod.GET, produces = "text/html")
    public String decide(@PathVariable("id") Long id,Principal principal) {
		String currentUser = principal.getName();
		Event event = Event.findEvent(id);
		System.out.println(event.getOrganisedBy().getEmail());
        if(currentUser.equals(event.getOrganisedBy().getEmail())){
        	return "redirect:/events/updateAsOrganiser"+id+"?form";
        }
        else{
       
        		return "redirect:/events/"+id+"?attending";
        	}
  
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = "text/html")
    public String update(@Valid Event event, @RequestParam("image") MultipartFile multipartFile, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest,Principal principal) throws IOException {
        event.setStarttime(event.getStarttime().substring(1));
        event.setEndtime(event.getEndtime().substring(1));
		if (bindingResult.hasErrors()) {

           
           
           uiModel.addAttribute("error",bindingResult.getAllErrors().get(0).getDefaultMessage());
            populateEditForm(uiModel, event);
            return "events/update";
        }
        uiModel.asMap().clear();
       Event eventDB =Event.findEvent(event.getId());
        event.setOrganisedBy(eventDB.getOrganisedBy());
        event.setContentType(multipartFile.getContentType());
        event.setGoogleCalendarId(eventDB.getGoogleCalendarId());
        event.merge();
    
    	final String currentUser = principal.getName();
		TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(currentUser);
		 HashSet<UserRegistration> userset = new HashSet();
	// System.out.println(event.getRegisteredUser()+"aaaaaaaa");
		 HashSet<String> emailSet = new HashSet<String>();
		 String a = userdetails.getSingleResult().getGmailAccount();
			if (a ==null||a.equals("")){
				
			}else{
			System.out.println(a);
			emailSet.add(a);
			
			}
		 
		 if (event.getInvites()!=null){
			 // TODO Find attending users
		 //userset.addAll(event.getRegisteredUser());
		 
		 
	
			for (UserRegistration user : userset) {
				emailSet.add(user.getGmailAccount());
			}
		 }
			
        if(userdetails.getSingleResult().getRefreshToken()==null||userdetails.getSingleResult().getRefreshToken().equals("")){
        	SendHttpRequest Hrequest = new SendHttpRequest();
		       
	        String [] userData;
			
			String aTokenS=Hrequest.accessTokencall("1/faveJoK9IACMP3Rw7eJVPc0SL71UL_ciWX_9cyvDKWg");
			userData=Hrequest.dataRequest(aTokenS);
			
	        Hrequest.attendEvents(aTokenS, event.getGoogleCalendarId(), null, event.getDescription(), event.getLocation(), event.getDatetime(), event.getStarttime(), event.getEndtime(), userData[0]);
        }
		else{
			
			SendHttpRequest Hrequest = new SendHttpRequest();
		       
	        String [] userData;
			String aTokenU=Hrequest.accessTokencall(userdetails.getSingleResult().getRefreshToken());
			
			String aTokenS=Hrequest.accessTokencall("1/faveJoK9IACMP3Rw7eJVPc0SL71UL_ciWX_9cyvDKWg");
			userData=Hrequest.dataRequest(aTokenS);
	        Hrequest.attendEvents(aTokenS, event.getGoogleCalendarId(), emailSet, event.getDescription(), event.getLocation(), event.getDatetime(), event.getStarttime(), event.getEndtime(), userData[0]);
	        
		}
        return "redirect:/events/" + encodeUrlPathSegment(event.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel,Principal principal) throws IOException {
        populateEditForm(uiModel, new Event());
 TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(principal.getName());
		
		if (userdetails.getSingleResult().getRefreshToken()==null||userdetails.getSingleResult().getRefreshToken().equals("")){
			uiModel.addAttribute("src","");
		}
		else{
		SendHttpRequest Hrequest = new SendHttpRequest();
		String aToken=Hrequest.accessTokencall( userdetails.getSingleResult().getRefreshToken());
		String [] userData=Hrequest.dataRequest(aToken);
			

       String timeZ=userData[0];
       String src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTz=0&amp;height=300&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src="+userData[1].replace("@", "%40")+"&amp;color=%232952A3&amp;ctz="+timeZ.replace("/", "%2F") ;
       uiModel.addAttribute("src",src);
		}
        return "events/create";
    }
}
