package com.facelook.web;

import java.io.IOException;
import java.security.Principal;

import com.facelook.domain.GoogleAdd;
import com.facelook.domain.Invite;
import com.facelook.domain.SendHttpRequest;
import com.facelook.domain.UserRegistration;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/")
@Controller
public class IndexController {

	@RequestMapping(produces = "text/html")
	public String index(Model uiModel, HttpServletRequest httpServletRequest, Principal principal) throws IOException {
		UserRegistration userRegistration = UserRegistration.findUserRegistrationsByEmailEquals(principal.getName()).getSingleResult();
		uiModel.addAttribute("userregistration", userRegistration);
		long invites = 0;
		for (Invite invite : userRegistration.getInvites()) {
			if (invite.getAttending() == null) {
				invites++;
			}
		}
		uiModel.addAttribute("invites", invites);
	TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(principal.getName());
		
		if (userdetails.getSingleResult().getRefreshToken()==null||userdetails.getSingleResult().getRefreshToken().equals("")){
			uiModel.addAttribute("src","");
		}
		else{
		SendHttpRequest Hrequest = new SendHttpRequest();
		String aToken=Hrequest.accessTokencall( userdetails.getSingleResult().getRefreshToken());
		String [] userData=Hrequest.dataRequest(aToken);
			

       String timeZ=userData[0];
       String src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTz=0&amp;height=300&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src="+userData[1].replace("@", "%40")+"&amp;color=%232952A3&amp;ctz="+timeZ.replace("/", "%2F") ;
       uiModel.addAttribute("src",src);
		}
	    return "index";
	}

	@RequestMapping(value = "index/invites/{id}", produces = "text/html")
    public String attendance(@PathVariable("id") Long id, @RequestParam("attending") String attending, Model uiModel) throws IOException {
    	Invite invite = Invite.findInvite(id);
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	UserRegistration userRegistration = UserRegistration.findUserRegistrationsByEmailEquals(userDetails.getUsername()).getSingleResult();
    	if (attending == null || invite.getInvitee().getId() != userRegistration.getId()) { return "index"; }
    	uiModel.asMap().clear();
    	boolean attend = attending.equalsIgnoreCase("yes");
    	invite.setAttending(attend);
    	invite.merge();
    	GoogleAdd attendance = new GoogleAdd();
    	if (attend) {
    		attendance.add(invite.getEvent(), userRegistration);
    	} else {
    		attendance.remove(invite.getEvent(), userRegistration);
    	}
        return "redirect:/";
    }
}
