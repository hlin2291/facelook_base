package com.facelook.web;
import com.facelook.domain.Invite;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;

@RequestMapping("/invites")
@Controller
@RooWebScaffold(path = "invites", formBackingObject = Invite.class)
@RooWebFinder
public class InviteController {
}
