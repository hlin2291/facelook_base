package com.facelook.web;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.HashSet;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.facelook.domain.Event;
import com.facelook.domain.SendHttpRequest;
import com.facelook.domain.UserRegistration;

@RequestMapping("/check/**")
@Controller
public class GoogleAccessController {

	private String accesstoken;
	private String request = "https://accounts.google.com/o/oauth2/token";
	private String timeZone ;

	@RequestMapping(params = "index", produces = "text/html")
    public String index(Principal principal) {
    	final String currentUser = principal.getName();
		TypedQuery<UserRegistration> userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(currentUser);
		 
		     String token =userdetails.getSingleResult().getRefreshToken();
		     if (token==null||token.equals("")){
		    	   return "redirect:https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=110258701403.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Ffacelook%2Fevents%3FmyEvents&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&access_type=offline";
		     }
		     else{
					return "redirect:/events?myEvents";
		     }
    }
	@RequestMapping(params = "refresh", produces = "text/html")
    public String refresh(@RequestParam(value = "code", required = false) String code,Principal principal,HttpServletRequest httpServletRequest) throws IOException {
		final String currentUser = principal.getName();
		UserRegistration userdetails =  UserRegistration.findUserRegistrationsByEmailEquals(currentUser).getSingleResult();
		
		  String urlParameters;

			
		urlParameters = "code="+code+"&client_id=110258701403.apps.googleusercontent.com&client_secret=QMZ8P3hZJ8z8z4NcIiI-hLSY&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Ffacelook%2Fcheck%3Frefresh&grant_type=authorization_code";
		
		
		
		SendHttpRequest Hrequest = new SendHttpRequest();
		
		String[] tokens=Hrequest.getAccessToken ( request, urlParameters);
		String refreshtoken= tokens[1];
		String[] emailData = Hrequest.dataRequest(tokens[0]);
		if (refreshtoken!=null){
			 userdetails.setRefreshToken(refreshtoken);
			 userdetails.setGmailAccount(emailData[1]);
			 userdetails.persist();
		}
  
      return "redirect:/userregistrations/" + encodeUrlPathSegment(userdetails.getId().toString(), httpServletRequest);
    }
	private String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}


