package com.facelook.web;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.facelook.domain.UserRegistration;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@RequestMapping("/adduser")
@Controller
public class AddUser {
	
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String createUser(@Valid UserRegistration userRegistration, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
    	uiModel.asMap().clear();
    	
    	//Hash the password
    	String password_unhashed = userRegistration.getPassword();
    	MessageDigest md;
    	StringBuffer sb = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password_unhashed.getBytes("UTF-8"));
	    	byte[] digest = md.digest();
	    	
	    	//convert the byte to hex format
	        
	        for (int i = 0; i < digest.length; i++) {
	          sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
	        }
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    	
		userRegistration.setPassword(sb.toString());
		userRegistration.setAuthenticated(true);
    	userRegistration.setRoleName("ROLE_USER");
    	
        userRegistration.persist();
        
        if (userRegistration.getAcccess()) {
			return "redirect:https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=110258701403.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Ffacelook%2Fcheck%3Frefresh&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&access_type=offline";
		} else {
			return "login";
		}
    }
    
    @RequestMapping(method = RequestMethod.GET, produces = "text/html")
    public String userSignUp(@Valid UserRegistration userRegistration, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
    	uiModel.addAttribute("userRegistration", userRegistration);
    	uiModel.addAttribute("userRegistration_dateofbirth_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    	return "adduser/index";
    }

}
