package com.facelook.web;
import com.facelook.domain.Event;
import com.facelook.domain.Interest;
import com.facelook.domain.Invite;
import com.facelook.domain.UserRegistration;
import java.util.HashSet;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;

@RequestMapping("/userregistrations")
@Controller
@RooWebScaffold(path = "userregistrations", formBackingObject = UserRegistration.class)
@RooWebFinder
public class UserRegistrationController {

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        UserRegistration userRegistration = UserRegistration.findUserRegistration(id);
        uiModel.addAttribute("userregistration", userRegistration);
        uiModel.addAttribute("itemId", id);
        return "userregistrations/show";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid UserRegistration userRegistration, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        boolean hasErrors = false;
        for (Object object : bindingResult.getAllErrors()) {
            if (object instanceof FieldError) {
                String field = ((FieldError) object).getField();
                if (field.equals("password") || field.equals("authenticated")) {
                    continue;
                }
            }
            hasErrors = true;
            break;
        }
        if (hasErrors) {
            populateEditForm(uiModel, userRegistration);
            return "userregistrations/update";
        }
        uiModel.asMap().clear();
        UserRegistration u = UserRegistration.findUserRegistration(userRegistration.getId());
        userRegistration.setPassword(u.getPassword());
        userRegistration.setAuthenticated(u.getAuthenticated());
        userRegistration.setRoleName(u.getRoleName());
        userRegistration.merge();
		if (userRegistration.getAcccess()) {
			return "redirect:https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=110258701403.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Ffacelook%2Fcheck%3Frefresh&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&access_type=offline";
		}
        return "redirect:/userregistrations/profile";
    }
    
    void populateEditForm(Model uiModel, UserRegistration userRegistration) {
        uiModel.addAttribute("userRegistration", userRegistration);
        uiModel.addAttribute("interests", Interest.findAllInterests());
        addDateTimeFormatPatterns(uiModel);
    }

    @RequestMapping(value = "/profile", produces = "text/html")
    public String profile(Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserRegistration userRegistration = UserRegistration.findUserRegistrationsByEmailEquals(userDetails.getUsername()).getSingleResult();
        populateEditForm(uiModel, userRegistration);
        System.out.println("profile");
        return "userregistrations/update";
    }
	  
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
	public String create(@Valid UserRegistration userRegistration,
			BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest) {
		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, userRegistration);
			return "userregistrations/create";
		}
		// System.out.println(userRegistration.getAcccess());

		uiModel.asMap().clear();

		userRegistration.persist();

		if (userRegistration.getAcccess()) {
			return "redirect:https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=110258701403.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Ffacelook%2Fcheck%3Frefresh&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&access_type=offline";
		} else {
			return "redirect:/userregistrations/"
					+ encodeUrlPathSegment(userRegistration.getId().toString(),
							httpServletRequest);
		}
	}

}
