package com.facelook.web;
import com.facelook.domain.Interest;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/interests")
@Controller
@RooWebScaffold(path = "interests", formBackingObject = Interest.class)
public class InterestController {
}
