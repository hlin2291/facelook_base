package com.facelook.domain;

import java.io.BufferedReader;
import java.io.StringReader;

import junit.framework.TestCase;

public class JsonTest extends TestCase{
public void testJsonGetTokens(){
	JsonReader read = new JsonReader ();
	
	String [] tokens =read.Reader(new BufferedReader(new StringReader("{token_type=Bearer, expires_in=3600, access_token=ya29.AHES6ZQuRoy-mL0UaFhacA21O7W3jUy3n8zjV9IrQJ8Wu6SoBPsjHw8}")));
	assertNull(tokens[1]);
	assertEquals("ya29.AHES6ZQuRoy-mL0UaFhacA21O7W3jUy3n8zjV9IrQJ8Wu6SoBPsjHw8",tokens[0]);
}
public void testJsonIdReader(){
	JsonReader read = new JsonReader ();
	String id = read.IdReader(new BufferedReader(new StringReader("{something=something, id=ME}")));
	assertEquals("ME",id);
}
public void testJsonGetTimeZone(){
	JsonReader read = new JsonReader ();
	String[] output = read.Reader2(new BufferedReader(new StringReader("{something=something,items=[{id=me,timeZone=time}]}")));
	assertEquals("me",output[1]);
	assertEquals("time",output[0]);
}
}

