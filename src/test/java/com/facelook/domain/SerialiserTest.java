package com.facelook.domain;


import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Test;

import junit.framework.TestCase;

public class SerialiserTest extends TestCase {


	public void testCreateEventJson() {
		JsonSerialize json = new JsonSerialize();
		Calendar c = Calendar.getInstance();
		c.set(2000, 10, 10);
		String output = json.JsonSerializerCreate("summary", "location", c, "starttime", "endtime","timeZone");
		assertEquals("{\"summary\":\"summary\",\"location\":\"location\",\"start\":{\"dateTime\":\"2000-11-10Tstarttime.00\",\"timeZone\":\"timeZone\"},\"end\":{\"dateTime\":\"2000-11-10Tendtime.00\",\"timeZone\":\"timeZone\"}}", output);
	}
	public void testDatetimeFormat(){
		JsonSerialize json = new JsonSerialize();
		Calendar c = Calendar.getInstance();
		c.set(2000, 10, 10);
		HashMap datetime =json.DatetimeFormat(c, "20:00", "timeZone");
		String time =(String) datetime.get("dateTime");
		String timeZone = (String) datetime.get("timeZone");
		assertEquals("timeZone",timeZone);
		assertEquals("2000-11-10T20:00.00",time);
		
		
	}
	public void testAddAttendees(){
		JsonSerialize json = new JsonSerialize();
		HashSet<String> emails = new HashSet<String>();
		emails.add("han");
		emails.add("david");
		Calendar c = Calendar.getInstance();
		c.set(2000, 10, 10);
		String test =json.JsonSerializerUpdate(emails, "summary", "location", c, "20:00", "23:00", "timeZone");
		assertEquals("{\"summary\":\"summary\",\"attendees\":[{\"email\":\"han\"},{\"email\":\"david\"}],\"location\":\"location\",\"start\":{\"dateTime\":\"2000-11-10T20:00.00\",\"timeZone\":\"timeZone\"},\"end\":{\"dateTime\":\"2000-11-10T23:00.00\",\"timeZone\":\"timeZone\"}}",test);
	}

}
