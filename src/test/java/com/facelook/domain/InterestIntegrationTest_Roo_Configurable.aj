// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.facelook.domain;

import com.facelook.domain.InterestIntegrationTest;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect InterestIntegrationTest_Roo_Configurable {
    
    declare @type: InterestIntegrationTest: @Configurable;
    
}
