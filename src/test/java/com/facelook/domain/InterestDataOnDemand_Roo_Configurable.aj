// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.facelook.domain;

import com.facelook.domain.InterestDataOnDemand;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect InterestDataOnDemand_Roo_Configurable {
    
    declare @type: InterestDataOnDemand: @Configurable;
    
}
